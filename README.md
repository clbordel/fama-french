# Fama French Factors Analysis - Capturing alpha using machine learning

## Nov 28, 2020

The purpose behind this project is to analyze the effectivness of the five Fama-French factor models in predicting returns on individual stock and ETF returns.

Various machine learning algorithms are used to predict stock/ETF returns. Stacking will be used to combine the best performing algorithms into one model/predictor.

Description of individual factors and data can be found at:

https://mba.tuck.dartmouth.edu/pages/faculty/ken.french/Data_Library/f-f_factors.html

In addition, individual historical portfolio returns were generated using TD Ameritrade's API:

https://developer.tdameritrade.com/blog/documentation

## March 17, 2021

The current state of these models/predictors is to predict continous data in a regression output. This is useful for predicting the movement of single stocks/ETFs.

Going forward, we will be switching to classification-based algorithms with the following labels:
1. "outperform" or
2. "underperform" 

Based on their cross-sectional return in a portfolio of ETF/Stock selections. The goal here is to identify the preferred stocks (i.e. the "outperformers" or those that rank in the top N (user defined) set of stocks).

A classification based approach is preferred to a regression based approach to reduce errors and provide better accuracy.s

# March 22, 2021

Better predictions may be made with additional factors and this data must be mined and formatted into the existing portfolio data when it occurs.

**Factor types to expand upon**

* deep value factors 
* relative value factors 
* factors focused on earnings quality 
* factors capturing earnings momentum
* factors focused on historical growth
* liquidity factors
* management quality factors
* profitability factors 
* technical price-based factors 

Continued hyperparamter optimization may improve some models.

# April 12th, 2021

The following screenshot shows preliminary base-line comparisons of several regression models used to predict ETF returns.

![](fama_french_v4/err-compare-20210412.png)

The top three performing models are:

1. Random Forest
2. XGBoost
3. Catboost/OLS
