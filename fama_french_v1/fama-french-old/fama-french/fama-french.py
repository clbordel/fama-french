import pandas as pd
import numpy as np 

import pandas_datareader.data as web

import statsmodels.formula.api as sm # module for stats models
from statsmodels.iolib.summary2 import summary_col # module for presenting stats models outputs nicely

def price2ret(prices,retType='simple'):
    if retType == 'simple':
        ret = (prices/prices.shift(1))-1
    else:
        ret = np.log(prices/prices.shift(1))
    return ret

# df_factors = web.DataReader('F-F_Research_Data_5_Factors_2x3_daily', 'famafrench')[0]
# print(df_factors.head)

stock_df = pd.read_csv("data/stk_historical_AAPL.csv",index_col='Date',parse_dates=True)
# print(stock_df.head)

stock_df = price2ret(stock_df,retType="NotSimple")
print(stock_df.head)